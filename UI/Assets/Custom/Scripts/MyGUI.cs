﻿using UnityEngine;
using System;

[ExecuteInEditMode]
public class MyGUI : MonoBehaviour {
	//[Serializable]
	public class ButtonSet {
		public Rect left = new Rect(5, 70, 50, 50);
		public Rect right = new Rect(105, 70, 50, 50);
		public Rect down = new Rect(55, 115, 50, 50);
		public Rect up = new Rect(55, 25, 50, 50);
		public Rect foward = new Rect(110, 5, 110, 50);
		public Rect back = new Rect(230, 5, 110, 50);

	}
		

	public ButtonSet guiButtons = new ButtonSet();

	void OnGUI() {
		GUI.Box (new Rect(5, 5, 100, 20), "Click Buttons");
		/*if (GUI.Button (new Rect (5, 25, 100, 20), "Click Me")) {
			t.position = new Vector3(3, 7, 5);
			Debug.Log("Change Position of this object to (3, 7, 5)");
		}*/

		if (GUI.RepeatButton (guiButtons.left, "Left")) {
			//t.position += Vector3.left * speed * Time.deltaTime;
			player.instance.Move(Vector3.left);
		}

		if (GUI.RepeatButton (guiButtons.right, "Right")) {
			//t.position += Vector3.right * speed * Time.deltaTime;
			player.instance.Move(Vector3.right);
		}

		if (GUI.RepeatButton (guiButtons.down, "Down")) {
			player.instance.Move(Vector3.down);
		}

		if (GUI.RepeatButton (guiButtons.up, "Up")) {
			player.instance.Move(Vector3.up);
		}

		if (GUI.RepeatButton (guiButtons.foward, "Foward")) {
			player.instance.Move(Vector3.forward);
		}

		if (GUI.RepeatButton (guiButtons.back, "Backward")) {
			player.instance.Move(Vector3.back);
		}
	}
}
